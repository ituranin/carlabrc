// Copyright (c) 2021 Igor Turanin, HTW Berlin Motorsport.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.

#pragma once

#include "carla/MsgPack.h"

#ifdef LIBCARLA_INCLUDED_FROM_UE4
#  include "Carla/Vehicle/VehicleSpeeds.h"
#endif // LIBCARLA_INCLUDED_FROM_UE4

namespace carla {
namespace rpc {

  class VehicleSpeeds {
  public:

    VehicleSpeeds() = default;

    VehicleSpeeds(
        float in_engine,
        float in_wheel_fl,
        float in_wheel_fr,
        float in_wheel_rl,
        float in_wheel_rr)
      : engine(in_engine),
        wheel_fl(in_wheel_fl),
        wheel_fr(in_wheel_fr),
        wheel_rl(in_wheel_rl),
        wheel_rr(in_wheel_rr) {}

    float engine = 0.0f;
    float wheel_fl = 0.0f;
    float wheel_fr = 0.0f;
    float wheel_rl = 0.0f;
    float wheel_rr = 0.0f;

#ifdef LIBCARLA_INCLUDED_FROM_UE4

    VehicleSpeeds(const FVehicleSpeeds &Speeds)
      : engine(Speeds.Engine),
        wheel_fl(Speeds.WheelFL),
        wheel_fr(Speeds.WheelFR),
        wheel_rl(Speeds.WheelRL),
        wheel_rr(Speeds.WheelRR) {}

    operator FVehicleSpeeds() const {
      FVehicleSpeeds Speeds;
      Speeds.Engine = engine;
      Speeds.WheelFL = wheel_fl;
      Speeds.WheelFR = wheel_fr;
      Speeds.WheelRL = wheel_rl;
      Speeds.WheelRR = wheel_rr;
      return Speeds;
    }

#endif // LIBCARLA_INCLUDED_FROM_UE4

    bool operator!=(const VehicleSpeeds &rhs) const {
      return
          engine != rhs.engine ||
          wheel_fl != rhs.wheel_fl ||
          wheel_fr != rhs.wheel_fr ||
          wheel_rl != rhs.wheel_rl ||
          wheel_rr != rhs.wheel_rr;
    }

    bool operator==(const VehicleSpeeds &rhs) const {
      return !(*this != rhs);
    }

    MSGPACK_DEFINE_ARRAY(
        engine,
        wheel_fl,
        wheel_fr,
        wheel_rl,
        wheel_rr);
  };

} // namespace rpc
} // namespace carla
