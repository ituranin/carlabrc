#include "TrackParserActor.h"


// Sets default values
ATrackParserActor::ATrackParserActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	// TEXT("/Game/Carla/Static/Props/Construction/SM_ConstructionCone")
	static ConstructorHelpers::FObjectFinder<UStaticMesh> yellowConeMeshAssetTemp(TEXT("/Game/Carla/Static/ConeYellow/ConeSmallYellow"));
	yellowConeMeshAsset = yellowConeMeshAssetTemp.Object;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> blueConeMeshAssetTemp(TEXT("/Game/Carla/Static/ConeBlue/ConeSmallBlue"));
	blueConeMeshAsset = blueConeMeshAssetTemp.Object;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> orangeConeMeshAssetTemp(TEXT("/Game/Carla/Static/ConeOrange/ConeBigOrange"));
	orangeConeMeshAsset = orangeConeMeshAssetTemp.Object;

	//static ConstructorHelpers::FObjectFinder<UMaterialInstanceConstant> groundMaterialTemp(*materials[FMath::RandRange(0, materials.Num() - 1)]);
	//groundMaterial = groundMaterialTemp.Object;

	SetActorLocation(FVector(0, 0, STANDARD_TRACK_MESH_Z));
	trackMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("TrackMesh"));
	trackMesh->bRenderInMainPass = false;
	trackMesh->bRenderInDepthPass = false;
	trackMesh->SetCastShadow(false);
	SetRootComponent(trackMesh);
	SetActorEnableCollision(false);
}

// Called when the game starts or when spawned
void ATrackParserActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATrackParserActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATrackParserActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	UE_LOG(LogCarla, Log, TEXT("TrackParser is HERE!!!!"));

	groundMaterial = LoadObject<UMaterialInstanceConstant>(NULL, *materials[FMath::RandRange(0, materials.Num() - 1)]);

	// TODO own method
	if (groundMaterial)
	{
		for(TActorIterator<AStaticMeshActor> actorItr(GetWorld()); actorItr; ++actorItr)
		{
			if (actorItr->GetName().Equals("GroundPlane") || actorItr->GetActorLabel().Equals("GroundPlane"))
			{
				actorItr->GetStaticMeshComponent()->SetMaterial(0, groundMaterial);
				break;
			}
		}
	}
	
	const FString JsonFilePath = FPaths::ProjectContentDir() + "Carla/Maps/Json/GeneratorMap.json";
	FString JsonString;

	FFileHelper::LoadFileToString(JsonString, *JsonFilePath);
	//UE_LOG(LogCarla, Warning, TEXT("JsonString: %s"), *JsonString);

	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(JsonString);
 
	if (FJsonSerializer::Deserialize(JsonReader, JsonObject) && JsonObject.IsValid())
	{
		GenerateTrackMesh(JsonObject);
		MoveSpawnPosition(JsonObject);
		AddCenterLineToRouteplaner(JsonObject);

		if (JsonObject->GetIntegerField("innerConesType") == BLUE_CONE_TYPE)
		{
			SpawnCone(JsonObject, "innerConesX", "innerConesY", blueConeMeshAsset, SMALL_CONE_MASS_KG, "InnerCone");
			SpawnCone(JsonObject, "outerConesX", "outerConesY", yellowConeMeshAsset, SMALL_CONE_MASS_KG, "OuterCone");
		}
		else
		{
			SpawnCone(JsonObject, "innerConesX", "innerConesY", yellowConeMeshAsset, SMALL_CONE_MASS_KG, "InnerCone");
			SpawnCone(JsonObject, "outerConesX", "outerConesY", blueConeMeshAsset, SMALL_CONE_MASS_KG, "OuterCone");
		}
		SpawnCone(JsonObject, "timeKeepingConesInnerX", "timeKeepingConesInnerY", orangeConeMeshAsset, BIG_CONE_MASS_KG, "TimeKeepingInnerCone");
		SpawnCone(JsonObject, "timeKeepingConesOuterX", "timeKeepingConesOuterY", orangeConeMeshAsset, BIG_CONE_MASS_KG, "TimeKeepingOuterCone");
		//SpawnCone(JsonObject, "startingLineX", "startingLineY", orangeConeMeshAsset, BIG_CONE_MASS_KG, "StartTimeKeepingOuterCone");
	}
}

void ATrackParserActor::GenerateTrackMesh(const TSharedPtr<FJsonObject> &JsonObject)
{
	const auto trackLineInnerX = JsonObject->GetArrayField("trackLineInnerY");
	const auto trackLineInnerY = JsonObject->GetArrayField("trackLineInnerX");
	const auto trackLineOuterX = JsonObject->GetArrayField("trackLineOuterY");
	const auto trackLineOuterY = JsonObject->GetArrayField("trackLineOuterX");

	TArray<FVector> vertices;
	TArray<int32> triangles;
	int vertexNum = 0;
	int length = trackLineInnerX.Num() - 1;

	for (size_t i = 0; i < length; i++)
	{
		vertices.Add(FVector(trackLineInnerX[i]->AsNumber(), trackLineInnerY[i]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 0
		vertices.Add(FVector(trackLineInnerX[i + 1]->AsNumber(), trackLineInnerY[i + 1]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 1
		vertices.Add(FVector(trackLineOuterX[i]->AsNumber(), trackLineOuterY[i]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 2
		vertices.Add(FVector(trackLineOuterX[i + 1]->AsNumber(), trackLineOuterY[i + 1]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 3

		triangles.Add(vertexNum);
		triangles.Add(vertexNum + 2);
		triangles.Add(vertexNum + 3);

		triangles.Add(vertexNum + 3);
		triangles.Add(vertexNum + 1);
		triangles.Add(vertexNum);

		vertexNum += 4; // increment by added vertices
	}

	vertices.Add(FVector(trackLineInnerX[length]->AsNumber(), trackLineInnerY[length]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 0
	vertices.Add(FVector(trackLineInnerX[0]->AsNumber(), trackLineInnerY[0]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 1
	vertices.Add(FVector(trackLineOuterX[length]->AsNumber(), trackLineOuterY[length]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 2
	vertices.Add(FVector(trackLineOuterX[0]->AsNumber(), trackLineOuterY[0]->AsNumber(), STANDARD_TRACK_MESH_Z)); // + 3

	triangles.Add(vertexNum);
	triangles.Add(vertexNum + 2);
	triangles.Add(vertexNum + 3);

	triangles.Add(vertexNum + 3);
	triangles.Add(vertexNum + 1);
	triangles.Add(vertexNum);

	TArray<FVector> normals;
	TArray<FVector2D> UV0;
	TArray<FProcMeshTangent> tangents;
	TArray<FLinearColor> vertexColors;

	trackMesh->CreateMeshSection_LinearColor(0, vertices, triangles, normals, UV0, vertexColors, tangents, true);
	
	trackMeshMaterial = LoadObject<UMaterialInstanceConstant>(NULL, *trackMeshMaterialString);
	trackMesh->SetMaterial(0, trackMeshMaterial);
}

void ATrackParserActor::MoveSpawnPosition(const TSharedPtr<FJsonObject> &JsonObject)
{
	AVehicleSpawnPoint *spawnPoint = nullptr;

	for (TActorIterator<AVehicleSpawnPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		spawnPoint = Cast<AVehicleSpawnPoint>(*ActorItr);
		if (spawnPoint->GetName().Equals("BRCSpawn"))
		{
			break;
		}
		else
		{
			spawnPoint = nullptr;
		}
	}

	if(spawnPoint == nullptr)
	{
		return;
	}

	const auto startingLineX = JsonObject->GetArrayField("startingLineX");
	const auto startingLineY = JsonObject->GetArrayField("startingLineY");
	const auto startingLineNormalX = JsonObject->GetArrayField("startingLineNormalX");
	const auto startingLineNormalY = JsonObject->GetArrayField("startingLineNormalY");

	FVector normalOrigin(startingLineNormalY[0]->AsNumber(), startingLineNormalX[0]->AsNumber(), 0.0F);
	FVector normalDestination(startingLineNormalY[1]->AsNumber(), startingLineNormalX[1]->AsNumber(), 0.0F);
	FVector headingVector(normalDestination.X - normalOrigin.X, normalDestination.Y - normalOrigin.Y, 0.0F);

	float distance = FVector::Distance(normalOrigin, normalDestination);
	FVector uNormalized = headingVector / headingVector.Size();

	FVector Location = normalOrigin - SPAWN_DISTANCE_FROM_STARTING_LINE * uNormalized;
	Location.Z = STANDARD_TRACK_MESH_Z;
	FRotator Rotation(0.0F, FMath::RadiansToDegrees(headingVector.HeadingAngle()), 0.0F);
	spawnPoint->SetActorLocation(Location);
	spawnPoint->SetActorRotation(Rotation);
}

void ATrackParserActor::AddCenterLineToRouteplaner(const TSharedPtr<FJsonObject> &JsonObject)
{
	// TODO
	const auto centerLineX = JsonObject->GetArrayField("trackLineCenterY");
	const auto centerLineY = JsonObject->GetArrayField("trackLineCenterX");

	TArray<FVector> waypoints;

	for (size_t i = 0; i < centerLineX.Num(); i++)
	{
		waypoints.Add(FVector(centerLineX[i]->AsNumber(), centerLineY[i]->AsNumber(), STANDARD_TRACK_MESH_Z));
	}

	FActorSpawnParameters SpawnParams;
	FVector Location(0.0F, 0.0F, STANDARD_TRACK_MESH_Z);
	FRotator Rotation(0.0F, 0.0F, 0.0F);
	ARoutePlanner* result = GetWorld()->SpawnActor<ARoutePlanner>(ARoutePlanner::StaticClass(), Location, Rotation, SpawnParams);
	FString name("RoutePlanner");
	result->Rename(*name);
	result->SetActorLabel(*name);
	result->SetBoxExtent(FVector(100000.0F, 100000.0F, 100.0F));
	result->AddRoute(PROPABILITY_TRACK_SELECTION, waypoints);
	
	// use for Debugging
	//result->DrawRoutes();
}

void ATrackParserActor::SpawnCone(const TSharedPtr<FJsonObject> &JsonObject, const FString &coneArrayXName, const FString &coneArrayYName, UStaticMesh *coneAsset, float massInKg, const FString name)
{
	const auto coneArrayX = JsonObject->GetArrayField(coneArrayYName);
	const auto coneArrayY = JsonObject->GetArrayField(coneArrayXName);

	for (size_t i = 0; i < coneArrayX.Num(); i++)
	{
		FVector Location(coneArrayX[i]->AsNumber(), coneArrayY[i]->AsNumber(), STANDARD_TRACK_MESH_Z);
		FRotator Rotation(0.0F, 0.0F, 0.0F);

		FActorSpawnParameters SpawnParams;
		AStaticMeshActor* result = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Location, Rotation, SpawnParams);
		FString newName(name);
		newName.AppendInt(i);
		result->Rename(*newName);
		result->SetActorLabel(*newName);

		UStaticMeshComponent* comp = result->GetStaticMeshComponent();
		if (comp)
		{
			comp->SetStaticMesh(coneAsset);
			comp->SetSimulatePhysics(true);
			comp->SetMassOverrideInKg(EName::NAME_None, massInKg);
		}
		result->SetMobility(EComponentMobility::Movable);
	}
}
