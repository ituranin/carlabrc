#pragma once

#include "Carla.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "Engine/StaticMeshActor.h"
#include "Vehicle/VehicleSpawnPoint.h"
#include "Traffic/RoutePlanner.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Materials/MaterialInstanceConstant.h"

#include "TrackParserActor.generated.h"

UCLASS()
class CARLA_API ATrackParserActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrackParserActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	TArray<FString> materials = {
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetAsphalt01.MI_WetAsphalt01'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetConcrete.MI_WetConcrete'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetGrass.MI_WetGrass'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetGrass_Inst.MI_WetGrass_Inst'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetRoad_1.MI_WetRoad_1'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetRoad_2.MI_WetRoad_2'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetSideWalk_1.MI_WetSideWalk_1'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetSidewalk_2.MI_WetSidewalk_2'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetSideWalk_3.MI_WetSideWalk_3'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetSideWalk_4.MI_WetSideWalk_4'",
		"MaterialInstanceConstant'/Game/Carla/Static/GenericMaterials/WetPavement/MI_WetSidewalk_5.MI_WetSidewalk_5'"
	};

	FString trackMeshMaterialString = "/Game/Carla/Static/GenericMaterials/MI_TwoSidedMesh";

	UStaticMesh *yellowConeMeshAsset = nullptr;
	UStaticMesh *blueConeMeshAsset = nullptr;
	UStaticMesh *orangeConeMeshAsset = nullptr;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent *trackMesh = nullptr;

	UMaterialInstanceConstant *groundMaterial = nullptr;
	UMaterialInstanceConstant *trackMeshMaterial = nullptr;

	const float BIG_CONE_MASS_KG = 1.05F;
	const float SMALL_CONE_MASS_KG = 0.45F;
	const float SPAWN_DISTANCE_FROM_STARTING_LINE = 154.0F;
	const float STANDARD_TRACK_MESH_Z = 1.0F;
	const float PROPABILITY_TRACK_SELECTION = 1.0F;
	const int BLUE_CONE_TYPE = 2;

	void GenerateTrackMesh(const TSharedPtr<FJsonObject> &JsonObject);
	void MoveSpawnPosition(const TSharedPtr<FJsonObject> &JsonObject);
	void AddCenterLineToRouteplaner(const TSharedPtr<FJsonObject> &JsonObject);
	void SpawnCone(const TSharedPtr<FJsonObject> &JsonObject, const FString &coneArrayXName, const FString &coneArrayYName, UStaticMesh *coneAsset, float massInKg, const FString name);
};
