// Copyright (c) 2021 Igor Turanin, HTW Berlin Motorsport.
//
// This work is licensed under the terms of the MIT license.
// For a copy, see <https://opensource.org/licenses/MIT>.

#pragma once

#include "VehicleSpeeds.generated.h"

USTRUCT(BlueprintType)
struct CARLA_API FVehicleSpeeds
{
  GENERATED_BODY()

  UPROPERTY(Category = "Vehicle Speeds", EditAnywhere, BlueprintReadOnly)
  float Engine = 0.0f;

  UPROPERTY(Category = "Vehicle Speeds", EditAnywhere, BlueprintReadOnly)
  float WheelFL = 0.0f;

  UPROPERTY(Category = "Vehicle Speeds", EditAnywhere, BlueprintReadOnly)
  float WheelFR = 0.0f;

  UPROPERTY(Category = "Vehicle Speeds", EditAnywhere, BlueprintReadOnly)
  float WheelRL = 0.0f;

  UPROPERTY(Category = "Vehicle Speeds", EditAnywhere, BlueprintReadOnly)
  float WheelRR = 0.0f;
};
