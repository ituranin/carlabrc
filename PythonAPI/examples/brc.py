# ==============================================================================
# -- find carla module ---------------------------------------------------------
# ==============================================================================


import glob
import os
import sys
import errno

try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

# ==============================================================================
# -- imports -------------------------------------------------------------------
# ==============================================================================

import carla

import shutil
import argparse
#import collections
#import datetime
import logging
import math
import random
import pandas
import numpy
import time
#import re
#import weakref

from PIL import Image

try:
    import pygame
    from pygame.locals import K_COMMA
    from pygame.locals import K_DOWN
    from pygame.locals import K_ESCAPE
    from pygame.locals import K_LEFT
    from pygame.locals import K_PERIOD
    from pygame.locals import K_RIGHT
    from pygame.locals import K_TAB
    from pygame.locals import K_UP
    from pygame.locals import K_m
    from pygame.locals import K_r
    from pygame.locals import K_p
except ImportError:
    raise RuntimeError('cannot import pygame, make sure pygame package is installed')

try:
    import numpy as np
except ImportError:
    raise RuntimeError('cannot import numpy, make sure numpy package is installed')


def to_bgra_array(image):
    """Convert a CARLA raw image to a BGRA numpy array."""
    #if not isinstance(image, sensor.Image):
    #    raise ValueError("Argument must be a carla.sensor.Image")
    array = numpy.frombuffer(image.raw_data, dtype=numpy.dtype("uint8"))
    array = numpy.reshape(array, (image.height, image.width, 4))
    return array

def to_rgb_array(image):
    """Convert a CARLA raw image to a RGB numpy array."""
    array = to_bgra_array(image)
    # Convert BGRA to RGB.
    array = array[:, :, :3]
    array = array[:, :, ::-1]
    return array

def createDirectory(path):
    directory = os.path.dirname(path)
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

class DynamicWeather(object):
    def __init__(self, world):
        pass

class DataPoint(object):
    def __init__(self, index, basePath):
        self.basePath = basePath
        self.index = index
        self.frame = None
        self.cameras = []
        self.lidars = []
        self.imus = []
        self.transform = None
        self.control = None
        self.velocity = None

    def getVehicleData(self):
        data = []
        data.append(self.frame)
        velocityKmh = (3.6 * math.sqrt(self.velocity.x**2 + self.velocity.y**2 + self.velocity.z**2))
        data.append(velocityKmh)
        data.append(self.control.throttle)
        data.append(self.control.brake)
        data.append(self.control.steer * 2500.0)
        data.append(self.control.gear)
        data.append(self.transform.location.x)
        data.append(self.transform.location.y)
        data.append(self.transform.location.z)
        data.append(self.transform.rotation.yaw)
        data.append(self.transform.rotation.pitch)
        data.append(self.transform.rotation.roll)
        return data

    def save(self):
        for index, image in enumerate(self.cameras):
            path = self.basePath + 'camera' + str(index) + '/' + str(image.frame)
            img = to_rgb_array(image)
            img = Image.fromarray(img)
            if index <= 5:
                img = img.crop((0, 380, 2048, 380 + 412))
            if index > 2:
                img.save(path + '.png')
            else:
                img.save(path + '.jpg')

class DataSet(object):
    def __init__(self, trackSeed, vehicle, basePath):
        self.trackSeed = trackSeed
        self.basePath = basePath + str(trackSeed) + '/'
        self.frameCount = 0
        self._vehicle = vehicle
        self.queue = None
        self._cameraCount = len(vehicle.cameras)
        self._lidarCount = len(vehicle.lidars)
        self._imuCount = len(vehicle.imus)
        self.current = None
        columns=['frame', 'velocityKmh', 'throttle', 'brake', 'steering', 'gear', 'locationX', 'locationY', 'locationZ', 'yaw', 'pitch', 'roll']
        imuColumns = ['frame', 'accelerationX', 'accelerationY', 'accelerationZ', 'roll', 'pitch', 'yaw', 'heading']
        self.dataFrame = pandas.DataFrame(columns=columns)
        self.imuDataFrames = []

        for i in range(self._cameraCount):
            createDirectory(self.basePath + 'camera' + str(i) + '/')

        for i in range(self._imuCount):
            self.imuDataFrames.append(pandas.DataFrame(columns=imuColumns))

    def checkSensors(self):
        for sensor in self._vehicle.cameras:
            if sensor.data is None:
                return False

        for sensor in self._vehicle.lidars:
            if sensor.data is None:
                return False

        for sensor in self._vehicle.imus:
            if sensor.data is None:
                return False
        
        return True

    def saveCurrent(self):
        self.current = DataPoint(self.frameCount, self.basePath)
        for camera in self._vehicle.cameras:
            self.current.cameras.append(camera.data)
            camera.data = None

        for index, imu in enumerate(self._vehicle.imus):
            imuFrame = pandas.DataFrame([imu.data], columns=self.imuDataFrames[index].columns)
            self.imuDataFrames[index].append(imuFrame, ignore_index=True)

        self.current.control = self._vehicle.actor.get_control()
        self.current.velocity = self._vehicle.actor.get_velocity()
        self.current.transform = self._vehicle.actor.get_transform()
        self.current.frame = self._vehicle.actor.get_world().get_snapshot().frame

        frameToAppend = pandas.DataFrame([self.current.getVehicleData()], columns=self.dataFrame.columns)
        self.dataFrame = self.dataFrame.append(frameToAppend, ignore_index=True)
        self.current.save()
        self.frameCount += 1

    def saveCSV(self):
        for index, frame in enumerate(self.imuDataFrames):
            frame.to_csv(self.basePath + 'imu' + str(index) + '.csv')
        self.dataFrame.to_csv(self.basePath + str(self.trackSeed) + '.csv')

class IMU(object):
    def __init__(self, transform):
        self.blueprintString = 'sensor.other.imu'
        self.transform = transform
        self.data = None
        self.actor = None

    def spawn(self, world, vehicle):
        blueprint = world.get_blueprint_library().find(self.blueprintString)
        self.actor = world.spawn_actor(blueprint, self.transform, attach_to=vehicle.actor, attachment_type=carla.AttachmentType.Rigid)
        self.actor.listen(self.callBack)

    def destroy(self):
        if self.actor is not None:
            self.actor.destroy()

    def callBack(self, event):
        temp = []
        temp.append(event.frame)
        temp.append(event.accelerometer.x)
        temp.append(event.accelerometer.y)
        temp.append(event.accelerometer.z)
        temp.append(event.gyroscope.x)
        temp.append(event.gyroscope.y)
        temp.append(event.gyroscope.z)
        temp.append(event.compass)
        self.data = temp

class Camera(object):
    def __init__(self, index, width, height, fov, transform, display=False, segmenation=False):
        self.display = display
        self.index = index
        self.actor = None
        self.blueprintString = 'sensor.camera.rgb'
        self.width = width
        self.height = height
        self.fov = fov
        self.transform = transform
        self.data = None
        self._dataSet = None
        self._vehicle = None
        self.segmentation = segmenation

        if segmenation:
            self.blueprintString = 'sensor.camera.semantic_segmentation'
    
    def spawn(self, world, vehicle=None, dataSet=None):
        if (dataSet is None and self.display is False) or (vehicle is None):
            return
        
        self._dataSet = dataSet
        self._vehicle = vehicle

        blueprint = world.get_blueprint_library().find(self.blueprintString)
        blueprint.set_attribute('image_size_x', str(self.width))
        blueprint.set_attribute('image_size_y', str(self.height))
        blueprint.set_attribute('fov', str(self.fov))

        #if self.segmentation == False:
        #    blueprint.set_attribute('slope', str(1.0))
        #    blueprint.set_attribute('toe', str(0.5))
        #    blueprint.set_attribute('shoulder', str(0.5))
        #    blueprint.set_attribute('blur_amount', str(0.0))

        self.actor = world.spawn_actor(blueprint, self.transform, attach_to=vehicle.actor, attachment_type=carla.AttachmentType.Rigid)
        self.actor.listen(self.callBack)

    def destroy(self):
        if self.actor is not None:
            self.actor.destroy()

    def callBack(self, event):
        self.data = event

        # TODO find something better
        if self.display:
            self._vehicle.image = event

class BaseVehicle(object):
    def __init__(self, world, vehicleFilter):
        self.blueprintFilter = vehicleFilter
        self.actor = None
        self.image = None
        self.fpsCamera = None
        self.cameras = []
        self.lidars = []
        self.imus = []
        self.__spawn(world)
        self.defineSensors()

    def defineSensors(self):
        pass

    def __spawn(self, world):
        blueprint = world.get_blueprint_library().find(self.blueprintFilter)

        blueprint.set_attribute('role_name', 'hero') # not sure if needed, keep for multi client scenario

        if blueprint.has_attribute('color'):
            color = random.choice(blueprint.get_attribute('color').recommended_values)
            blueprint.set_attribute('color', color)

        if blueprint.has_attribute('is_invincible'):
            blueprint.set_attribute('is_invincible', 'true')
        
        worldMap = world.get_map()
        
        while self.actor is None:
            spawnPoints = worldMap.get_spawn_points()
            spawnPoint = random.choice(spawnPoints) if spawnPoints else carla.Transform()
            self.actor = world.try_spawn_actor(blueprint, spawnPoint)

    def initSensors(self, world, dataSet):
        for camera in self.cameras:
            camera.spawn(world, self, dataSet)
        
        for lidar in self.lidars:
            lidar.spawn(world, self, dataSet)

        for imu in self.imus:
            imu.spawn(world, self)

    def destroy(self):
        for camera in self.cameras:
            camera.destroy()
        
        for lidar in self.lidars:
            lidar.destroy()

        for imu in self.imus:
            imu.destroy()
        
        self.actor.destroy()

class BRC19(BaseVehicle):
    def __init__(self, world):
        super().__init__(world, 'vehicle.htw motorsport.brc19')

    def defineSensors(self):
        self.cameras.append(Camera(0, 1280, 720, 94.8, carla.Transform(carla.Location(x=-0.39, z=0.92), carla.Rotation(pitch=-10.0)), True, False))
        # only available since CARLA 0.9.7 :(
        #self.imus.append(IMU(carla.Transform(carla.Location(x=0.0, y= 0.0,z=0.0), carla.Rotation())))

class BRC19MasterThesis(BaseVehicle):
    def __init__(self, world):
        super().__init__(world, 'vehicle.htw motorsport.brc19') # 'vehicle.htw motorsport.brc20'

    def defineSensors(self):
        #11.2x, 20z, 94.8deg
        stereoOffset = 0.725 # 0.15 stereoCam # 0.725 car width
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, z=0.92), carla.Rotation(pitch=-10.0)), False, False))
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, y=-stereoOffset, z=0.92), carla.Rotation(pitch=-10.0)), False, False))
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, y=stereoOffset, z=0.92), carla.Rotation(pitch=-10.0)), False, False))
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, z=0.92), carla.Rotation(pitch=-10.0)), False, True))
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, y=-stereoOffset, z=0.92), carla.Rotation(pitch=-10.0)), False, True))
        self.cameras.append(Camera(0, 2048, 1088, 94.8, carla.Transform(carla.Location(x=-0.39, y=stereoOffset, z=0.92), carla.Rotation(pitch=-10.0)), False, True))
        #self.cameras.append(Camera(0, 1024, 1024, 60.0, carla.Transform(carla.Location(x=16.4, z=26), carla.Rotation(pitch=-90.0)), False, True))
        self.cameras.append(Camera(0, 1024, 1024, 10.0, carla.Transform(carla.Location(x=16.5, z=26*6.6), carla.Rotation(pitch=-90.0)), False, True))
        self.cameras.append(Camera(0, 1024, 1024, 10.0, carla.Transform(carla.Location(x=16.5, y=-stereoOffset, z=26*6.6), carla.Rotation(pitch=-90.0)), False, True))
        self.cameras.append(Camera(0, 1024, 1024, 10.0, carla.Transform(carla.Location(x=16.5, y=stereoOffset, z=26*6.6), carla.Rotation(pitch=-90.0)), False, True))
        # only available since CARLA 0.9.7 :(
        # there should be two imu's but without a 3d model of the vehicle it is not possible to measure a relative position of the sensors
        #self.imus.append(IMU(carla.Transform(carla.Location(x=0.0, y= 0.0,z=0.0), carla.Rotation())))

class KeyHandler(object):
    def __init__(self, vehicle, autopilot):
        self._control = carla.VehicleControl()
        self._vehicle = vehicle.actor
        self._steer_cache = 0
        self._autopilot_enabled = autopilot
        self._vehicle.set_autopilot(self._autopilot_enabled)

    def _toggleAutopilot(self):
        self._autopilot_enabled = not self._autopilot_enabled
        self._vehicle.set_autopilot(self._autopilot_enabled)

    def _parse_vehicle_keys(self, keys, milliseconds):
        self._control.throttle = 0.8 if keys[K_UP] else 0.0 # here 1.0 if up
        steer_increment = 5e-2 * milliseconds # here 5e-4
        if keys[K_LEFT]:
            self._steer_cache -= steer_increment
        elif keys[K_RIGHT]:
            self._steer_cache += steer_increment
        else:
            self._steer_cache = 0.0
        self._steer_cache = min(0.7, max(-0.7, self._steer_cache))
        self._control.steer = round(self._steer_cache, 1)
        self._control.brake = 1.0 if keys[K_DOWN] else 0.0

    # handle user input
    def parse_events(self, clock):
        for event in pygame.event.get():
            if event == pygame.QUIT:
                return True
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    return True
                elif event.key == K_m:
                    self._control.manual_gear_shift = not self._control.manual_gear_shift
                    self._control.gear = self._vehicle.get_control().gear
                elif self._control.manual_gear_shift and event.key == K_COMMA:
                    self._control.gear = max(0, self._control.gear - 1)
                elif self._control.manual_gear_shift and event.key == K_PERIOD:
                    self._control.gear = max(self._control.gear + 1, 4)
                elif event.key == K_p:
                    self._toggleAutopilot()
        self._parse_vehicle_keys(pygame.key.get_pressed(), clock.get_time())
        self._vehicle.apply_control(self._control)

class DynamicWeatherParameter(object):
    def __init__(self, minValue, maxValue, frames, randomDisable=False):
        self.__min = minValue
        self.__max = maxValue
        self.__disable = False

        # * 2.0 because we want to complete walkthroughs of the value from min to max
        self.__step = maxValue * 2.0 / frames
        self.value = numpy.random.random() * self.__max

        # useful for rain, because values > 0 equal rain
        if randomDisable and numpy.random.randint(2):
            self.__disable = True
            self.value = self.__min

        self.__direction = -1
        if numpy.random.randint(2):
            self.__direction = 1
        self.__step = self.__step * self.__direction
        
        self.__toggleDirection = False
        if numpy.random.randint(2):
            self.__toggleDirection = True

    def runStep(self):
        if self.__disable:
            return
        
        temp = self.value + self.__step
        if temp < self.__max and temp > self.__min:
            self.value = temp
        else:
            if self.__toggleDirection:
                self.__step *= -1
                self.value = self.value + self.__step
            else:
                if self.__direction == 1:
                    self.value = self.__min
                else:
                    self.value = self.__max

class DynamicWeather(object):
    def __init__(self, fps, maxFrames):
        self.frames = maxFrames
        
        self.__cloudyness = DynamicWeatherParameter(0.0, 100.0, self.frames)
        self.__precipitation = DynamicWeatherParameter(0.0, 100.0, self.frames, True)
        self.__precipitation_deposits = DynamicWeatherParameter(0.0, 100.0, self.frames)
        self.__wind_intensity = DynamicWeatherParameter(0.0, 100.0, self.frames)
        self.__wetness = DynamicWeatherParameter(0.0, 100.0, self.frames)
        self.__sun_azimuth_angle = DynamicWeatherParameter(0.0, 360.0, self.frames)
        self.__sun_altitude_angle = DynamicWeatherParameter(0.0, 90.0, self.frames)
        
        self.outputParameters = carla.WeatherParameters()
        self.__setOutput()

    def __setOutput(self):
        self.outputParameters.cloudyness = self.__cloudyness.value
        self.outputParameters.precipitation = 0.0 # disable rain # self.__precipitation.value
        self.outputParameters.precipitation_deposits = 0.0 #self.__precipitation_deposits.value
        self.outputParameters.wind_intensity = self.__wind_intensity.value
        self.outputParameters.wetness = self.__wetness.value
        self.outputParameters.sun_azimuth_angle = self.__sun_azimuth_angle.value
        self.outputParameters.sun_altitude_angle = self.__sun_altitude_angle.value

    def runStep(self):
        self.__cloudyness.runStep()
        self.__precipitation.runStep()
        self.__precipitation_deposits.runStep()
        self.__wind_intensity.runStep()
        self.__wetness.runStep()
        self.__sun_azimuth_angle.runStep()
        self.__sun_altitude_angle.runStep()
        self.__setOutput()

# ==============================================================================
# -- script state --------------------------------------------------------------
# ==============================================================================

TRACK_DIR = '/mnt/1547513F5927D680/CARLA/ma-src/tracks_3m_5m_oddshort/'
CARLA_DIR = '/mnt/1547513F5927D680/CARLA/carlabrc/'
GENERATOR_MAP_JSON_DIR = 'Unreal/CarlaUE4/Content/Carla/Maps/Json/'
GENERATOR_MAP_JSON_NAME = 'GeneratorMap.json'
DATASET_DST_DIR = '/mnt/1547513F5927D680/SimData_3m5m_short/'
FRAMES_TO_GET = 1000

class ScriptState(object):
    def __init__(self, client, trackSeed, args):
        self.client = client
        self.trackSeed = trackSeed
        self.args = args
        self.load_map(client, trackSeed)
        self.world = client.get_world()
        
        if args.sync:
            self.setSynchronous(args.fps)
        else:
            self.setAsynchronous()

        self.vehicle = BRC19MasterThesis(self.world)
        self.dataSet = DataSet(self.trackSeed, self.vehicle, DATASET_DST_DIR)
        self.vehicle.initSensors(self.world, self.dataSet)

        self.keyHandler = KeyHandler(self.vehicle, args.autopilot)

        # for repeatability
        numpy.random.seed(self.trackSeed)

        self.weather = DynamicWeather(args.fps, FRAMES_TO_GET)
        self.world.set_weather(self.weather.outputParameters)

    def setSynchronous(self, fps):
        worldSettings = self.world.get_settings()
        worldSettings.synchronous_mode = True
        worldSettings.fixed_delta_seconds = 1 / fps # ms
        self.world.apply_settings(worldSettings)

    def setAsynchronous(self):
        worldSettings = self.world.get_settings()
        worldSettings.synchronous_mode = False
        worldSettings.fixed_delta_seconds = None
        self.world.apply_settings(worldSettings)

    def load_map(self, client, trackSeed):
        src = TRACK_DIR + str(trackSeed) + '.json'
        dst = CARLA_DIR + GENERATOR_MAP_JSON_DIR + GENERATOR_MAP_JSON_NAME
        shutil.copy(src, dst)
        client.load_world('GeneratorMap')

    def destroy(self):
        self.vehicle.destroy()

        # do this for the server to not freeze
        self.setAsynchronous()
        pass

# ==============================================================================
# -- static functions ----------------------------------------------------------
# ==============================================================================

# convert image to pygame surface
def convert_image(image, display):
    if image is None:
        return
    array = np.frombuffer(image.raw_data, dtype=np.dtype("uint8"))
    array = np.reshape(array, (image.height, image.width, 4))
    array = array[:, :, :3]
    array = array[:, :, ::-1]
    display.blit(pygame.surfarray.make_surface(array.swapaxes(0, 1)), (0, 0))

def game_loop(args):
    pygame.init()
    pygame.font.init()

    trackSeed = 0 # 22 - curvy at start, 20 - initial test track, 45
    todo = 11 - (trackSeed + 1)
    runs = 0
    state = None

    try:
        client = carla.Client(args.host, args.port)
        client.set_timeout(100.0)

        state = ScriptState(client, trackSeed, args)

        #if not args.autopilot:
        display = pygame.display.set_mode((args.width, args.height), pygame.HWSURFACE | pygame.DOUBLEBUF)
        
        clock = pygame.time.Clock()
        
        while True:
            if state.dataSet.frameCount == FRAMES_TO_GET:
                state.dataSet.saveCSV()
                state.destroy()
                runs += 1

                if runs == todo:
                    state = None
                    break

                trackSeed += 1
                state = ScriptState(client, trackSeed, args)

            if args.sync:
                state.world.tick()
                while True:
                    if state.dataSet.checkSensors():
                        state.dataSet.saveCurrent()
                        break
                state.weather.runStep()
                state.world.set_weather(state.weather.outputParameters)
            else:
                state.weather.runStep()
                state.world.set_weather(state.weather.outputParameters)
                clock.tick_busy_loop(args.fps)

            if state.keyHandler.parse_events(clock):
                return
            convert_image(state.vehicle.image, display)
            pygame.display.flip()
    finally:
        if state is not None:
            state.destroy()
        
        pygame.quit()


# ==============================================================================
# -- main() --------------------------------------------------------------------
# ==============================================================================


def main():
    argparser = argparse.ArgumentParser(
        description='BRC Data Gathering')
    argparser.add_argument(
        '-v', '--verbose',
        action='store_true',
        dest='debug',
        help='print debug information')
    argparser.add_argument(
        '--host',
        metavar='H',
        default='127.0.0.1',
        help='IP of the host server (default: 127.0.0.1)')
    argparser.add_argument(
        '-p', '--port',
        metavar='P',
        default=2000,
        type=int,
        help='TCP port to listen to (default: 2000)')
    argparser.add_argument(
        '-a', '--autopilot',
        action='store_true',
        help='enable autopilot')
    argparser.add_argument(
        '--res',
        metavar='WIDTHxHEIGHT',
        default='128x128',
        help='window resolution (default: 1280x720)')
    argparser.add_argument(
        '--filter',
        metavar='PATTERN',
        default='vehicle.htw*',
        help='actor filter (default: "vehicle.htw*")')
    argparser.add_argument(
        '--rolename',
        metavar='NAME',
        default='hero',
        help='actor role name (default: "hero")')
    argparser.add_argument(
        '--gamma',
        default=2.2,
        type=float,
        help='Gamma correction of the camera (default: 2.2)')
    argparser.add_argument(
        '-s', '--sync',
        action='store_true',
        help='enable synchronous mode')
    argparser.add_argument(
        '--fps',
        default=10.0,
        type=float,
        help='FPS of simulation (default: 10.0)')
    argparser.add_argument(
        '--runs',
        default=1,
        type=int,
        help='datasets to collect (default: 1)')
    args = argparser.parse_args()

    args.width, args.height = [int(x) for x in args.res.split('x')]

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=log_level)

    logging.info('listening to server %s:%s', args.host, args.port)

    print(__doc__)

    try:
        game_loop(args)

    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')


if __name__ == '__main__':

    main()